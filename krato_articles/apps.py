from django.apps import AppConfig


class KratoArticlesConfig(AppConfig):
    name = 'krato_articles'
