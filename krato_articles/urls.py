from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import product_list, product_item

urlpatterns = [
    url(r'^krato_articles/tienda/(?P<shop_pk>[0-9]+)/products/$', product_list.as_view()),
    url(r'^krato_articles/tienda/(?P<shop_pk>[0-9]+)/products/(?P<product_pk>[0-9]+)$', product_item.as_view()),
] 