# Create your views here.
from rest_framework import generics
from django.http import Http404
from django.shortcuts import get_object_or_404, get_list_or_404
from .models import Shop, Product
from .serializers import ShopSerializer, ProductSerializer

class product_list(generics.ListAPIView):
    serializer_class = ProductSerializer
    lookup_field = 'shop_pk'
    filter_field = 'shops__id'

    def get_queryset(self):
        filter = {}
        filter[self.filter_field] = self.kwargs.get(self.lookup_field)
        return get_list_or_404(Product.objects.all(), **filter)

class product_item(generics.RetrieveAPIView):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    multiple_lookup_fields = ['shop_pk', 'product_pk']
    multiple_filter_fields = ['shops__id', 'id']

    def get_object(self):
        queryset = self.get_queryset()
        filter ={}
        for filter_field, lookup_field in zip(self.multiple_filter_fields, self.multiple_lookup_fields): 
            filter[filter_field] = self.kwargs.get(lookup_field)

        return get_object_or_404(queryset, **filter)
