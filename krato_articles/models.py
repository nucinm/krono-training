from django.db import models

# Create your models here.
class Shop(models.Model):
    name = models.CharField(max_length=30, unique=True)
    address = models.CharField(max_length=30)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]

class Product(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=100)
    image = models.URLField(max_length=400)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    shops = models.ManyToManyField(Shop)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name", "price"]